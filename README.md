Module Shipping Insurance for Magento 1 (Was created in magento-1.9.3.4).

The module can be configured in the admin panel (System->Configuration tab "sales").

You can use the absolute value of the cost of insurance or a percentage of the cost of the order.

Shipping Insurance is added on the checkout form, after choosing the delivery method.

Also Shipping Insurance is added to total, invoices etc.